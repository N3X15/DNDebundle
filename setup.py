import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="dndebundle", # Replace with your own username
    version="0.0.1",
    author="Rob Nelson",
    author_email="nexisentertainment@gmail.com",
    description="A tool for safely forensically analyzing .NET Core bundles.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    license='MIT',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    entry_points={
        'console_scripts': [
            'dndebundle = dndebundle.__main__:main'
        ]
    }
)
