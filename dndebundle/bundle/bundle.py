# https://github.com/dotnet/core-setup/blob/master/src/corehost/cli/apphost/bundle/extractor.cpp
import os, binascii, tqdm
from typing import BinaryIO, Optional
from pycsbinarywriter import cstypes

from dndebundle.bundle.fileentry import FileEntry
from dndebundle.bundle.header import BundleHeader

class NotABundleException(Exception):
    pass

class Bundle(object):
    # SHA-256 of '.net core bundle', used to tell AppHost that a file:
    # 1. Is a bundle.
    # 2. Where the offset is stored.
    #
    #  The 8 bytes preceding this is 0 by default, but is changed to the offset
    # of the bundle header if the file is a bundle.
    #
    # Yes, microsoft love convoluted solutions to simple problems. Sigh.
    HEADER_MARKER = bytes([0x8b, 0x12, 0x02, 0xb9, 0x6a, 0x61, 0x20, 0x38,
                           0x72, 0x7b, 0x93, 0x02, 0x14, 0xd7, 0xa0, 0x32,
                           0x13, 0xf5, 0xb9, 0xe6, 0xef, 0xae, 0x33, 0x18,
                           0xee, 0x3b, 0x2d, 0xce, 0x24, 0xb3, 0x6a, 0xae])

    @classmethod
    def IsBundle(cls, filename: str) -> bool:
        with open(filename, 'rb') as f:
            marker_pos = f.read().find(cls.HEADER_MARKER)
            # Check if we even found the marker.
            if marker_pos == -1:
                return False
            # Move 8 positions back.
            f.seek(marker_pos-8)
            # Read header offset from marker prefix.
            header_offset = cstypes.int64.unpackFrom(f)
            #  0 = non-self-contained executable.
            # >0 = R2R/Self-contained executable.
            # This is saved as an int64, so if we're negative, throw an error.
            assert header_offset >= 0
            return header_offset > 0

    def __init__(self, filename: str) -> None:
        self.filename: str = filename
        self.header: BundleHeader = None

        # Position in the file for the 128-bit marker.
        self.marker_offset: int = 0
        # Position in the file at which the Bundle header starts.
        self.header_offset: int = 0

    def _extractFile(self, entry: FileEntry, reader: BinaryIO, to_dir: str) -> None:
        '''
        extractor_t::extract
        '''
        filename: str = os.path.join(to_dir, entry.path)
        filedir: str = os.path.dirname(filename)
        if not os.path.isdir(filedir):
            os.makedirs(filedir)
        with open(filename+'.tmp', 'wb') as f:
            reader.seek(entry.offset, 0)
            f.write(reader.read(entry.size))
        #if os.path.isfile(filename):
        #    os.remove(filename)
        os.replace(filename+'.tmp', filename)
        if os.path.isfile(filename+'.tmp'):
            os.remove(filename+'.tmp')

    def extractTo(self, to_dir: str, use_tqdm: bool = False) -> None:
        self.loadMetadata()
        with open(self.filename, 'rb') as f:
            it = self.header.files.values()
            if use_tqdm:
                it = tqdm.tqdm(self.header.files.values(), 'Extracting...', ascii=True, unit='file')
            for fileEntry in it:
                self._extractFile(fileEntry, f, to_dir)

    def loadMetadata(self, verbose: bool = True) -> None:
        size = os.path.getsize(self.filename)
        with open(self.filename, 'rb') as f:
            self.reader = f
            self.marker_offset = f.read().find(self.HEADER_MARKER)
            # Check if we even *have* a marker.
            if self.marker_offset == -1:
                raise NotABundleException()
            if verbose: print(f'Found marker at {self.marker_offset} ({self.marker_offset:X})')
            # Move 8 positions back.
            f.seek(self.marker_offset-8)
            # Read header position from marker prefix.
            self.header_offset = cstypes.int64.unpackFrom(f)
            #  0 = non-self-contained executable.
            # >0 = R2R/Self-contained executable.
            # This is saved as an int64, so if we're negative, throw an error.
            assert self.header_offset >= 0
            if self.header_offset == 0:
                # Standard .NET Core AppHost, not a bundle.
                raise NotABundleException()
            if verbose: print(f'Header offset at {self.header_offset} ({self.header_offset:X})')
            assert self.header_offset < size
            # Seek to the header.
            f.seek(self.header_offset, 0)
            # Read it.
            self.header = BundleHeader()
            self.header.deserialize(f)
