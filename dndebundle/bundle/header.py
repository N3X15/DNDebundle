import hashlib
from typing import BinaryIO, Tuple, Optional, Dict
from pycsbinarywriter import cstypes
# https://github.com/dotnet/runtime/blob/master/src/installer/managed/Microsoft.NET.HostModel/Bundle/Manifest.cs#L116
from dndebundle.bundle.efiletype import EFileType
from dndebundle.bundle.fileentry import FileEntry
from dndebundle.bundle.header_flags import EHeaderFlags
class BundleHeader(object):
    VERSION = (2, 0)
    def __init__(self) -> None:
        self.version: Tuple[int, int] = (2, 0)
        self.file_count: int = 0
        self.id: str = ''
        self.deps_offset: Optional[int] = None
        self.deps_size: Optional[int] = None
        self.runtime_cfg_offset: Optional[int] = None
        self.runtime_cfg_size: Optional[int] = None
        self.flags: EHeaderFlags = EHeaderFlags.NONE

        self.files: Dict[str, FileEntry] = {}

    def is_valid(self) -> bool:
        return self.file_count > 0 and self.version <= self.VERSION and self.id != ''

    def deserialize(self, rdr: BinaryIO) -> None:
        major: int = cstypes.int32.unpackFrom(rdr)
        minor: int = cstypes.int32.unpackFrom(rdr)
        self.version = (major, minor)
        self.file_count = cstypes.int32.unpackFrom(rdr)
        self.id = cstypes.string.unpackFrom(rdr)
        if self.version[0] >= 2:
            self.deps_offset = cstypes.int64.unpackFrom(rdr)
            self.deps_size = cstypes.int64.unpackFrom(rdr)
            self.runtime_cfg_offset = cstypes.int64.unpackFrom(rdr)
            self.runtime_cfg_size = cstypes.int64.unpackFrom(rdr)
            self.flags = EHeaderFlags(cstypes.uint64.unpackFrom(rdr))
        assert self.is_valid(), 'Invalid BundleHeader detected.'
        for _ in range(self.file_count):
            fe = FileEntry()
            fe.deserialize(rdr)
            self.files[fe.path] = fe

    def print(self, filename_for_hashing: Optional[str] = None) -> None:
        major, minor = self.version
        print(f'Version {major}.{minor}')
        print(f'ID: {self.id}')
        if self.version > (1, 0):
            print('Offsets:')
            print(f'  .deps.json:          {self.deps_size:,}B @{self.deps_offset:X}')
            print(f'  .runtimeconfig.json: {self.runtime_cfg_size:,}B @{self.runtime_cfg_offset:X}')
            print('Flags:')
            for value in self.flags:
                print(f'  - {value.name}')
        print(f'Files: {self.file_count:,}')
        with open(filename_for_hashing, 'rb') as f:
            for fe in self.files.values():
                hashvalue = (' - SHA256='+fe.calcHash(hashlib.sha256, f)) if filename_for_hashing is not None else ''
                print(str(fe)+hashvalue)
