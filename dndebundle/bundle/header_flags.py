import enum
#https://github.com/dotnet/runtime/blob/master/src/installer/managed/Microsoft.NET.HostModel/Bundle/Manifest.cs#L57
class EHeaderFlags(enum.IntEnum):
    NONE                      = 0
    NETCORE_APP_3_COMPAT_MODE = 1
