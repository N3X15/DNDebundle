from typing import BinaryIO, Union
from pycsbinarywriter import cstypes
# https://github.com/dotnet/core-setup/blob/master/src/corehost/cli/apphost/bundle/file_entry.h
from dndebundle.bundle.efiletype import EFileType
class FileEntry(object):
    '''
    FileEntry: Records information about embedded files.

    The bundle manifest records the following meta-data for each
    file embedded in the bundle:
    Fixed size portion (file_entry_fixed_t)
      - Offset
      - Size
      - File Entry Type
    Variable Size portion
      - relative path (7-bit extension encoded length prefixed string)
    '''
    def __init__(self) -> None:
        self.offset: int = 0
        self.size: int = 0
        self.type: EFileType = EFileType(0)
        self.path: str = 0

    def is_valid(self) -> bool:
        return self.offset > 0 and self.size > 0 and self.type.value < EFileType.LAST__.value

    def deserialize(self, rdr: BinaryIO) -> None:
        self.offset = cstypes.int64.unpackFrom(rdr)
        self.size = cstypes.int64.unpackFrom(rdr)
        self.type = EFileType(int.from_bytes(rdr.read(1), 'big'))
        assert self.is_valid(), 'Invalid FileEntry detected.'
        self.path = cstypes.string.unpackFrom(rdr)

    def calcHash(self, algo, f: Union[str, BinaryIO]) -> str:
        if isinstance(f, str):
            with open(f, 'rb') as rdr:
                return self.calcHash(algo, rdr)
        else:
            f.seek(self.offset, 0)
            return algo(f.read(self.size)).hexdigest()

    def __str__(self) -> str:
        return f'{self.path} [{self.type.name}] @{self.offset} Sz={self.size}'
