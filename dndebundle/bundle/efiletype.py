from enum import IntEnum, auto
# https://github.com/dotnet/core-setup/blob/master/src/corehost/cli/apphost/bundle/file_type.h
class EFileType(IntEnum):
    ASSEMBLY = 0
    READY2RUN = 1
    DEPS_JSON = 2
    RUNTIME_CONFIG_JSON = 3
    EXTRACT = 4
    LAST__ = 5
