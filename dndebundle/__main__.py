from dndebundle.bundle.bundle import Bundle
def main():
    import argparse

    argp = argparse.ArgumentParser(description='.NET Core Bundle Forensic Analysis Utility')
    subp = argp.add_subparsers()

    extractp = subp.add_parser('extract', help='Extract the contents of a .NET Core Bundle')
    extractp.add_argument('exefile', type=str, help='Path to the executable.')
    extractp.add_argument('destdir', type=str, help='Destination directory where the contents will be extracted.')
    extractp.add_argument('--no-progress', dest='progress', action='store_false', default=True, help='Whether to use tqdm to display a progress bar.')
    extractp.set_defaults(cmd=_cmd_extract)

    infop = subp.add_parser('info', help='List information about a .NET Core Bundle.')
    infop.add_argument('exefile', type=str, help='Path to the executable.')
    infop.set_defaults(cmd=_cmd_info)

    args = argp.parse_args()
    cmd = getattr(args, 'cmd', None)
    if cmd is None:
        argp.print_help()
    else:
        args.cmd(args)

def _cmd_extract(args):
    assert Bundle.IsBundle(args.exefile)
    Bundle(args.exefile).extractTo(args.destdir, use_tqdm=args.progress)

def _cmd_info(args):
    if not Bundle.IsBundle(args.exefile):
        print('Not a bundle.')
    else:
        print('.NET Core Bundle')
        b = Bundle(args.exefile)
        b.loadMetadata()
        b.header.print(args.exefile)

if __name__ == '__main__': main()
