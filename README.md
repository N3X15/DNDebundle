# DNDebundle

.NET Core bundles are self-contained executables created by running
`dotnet publish -r ${PLATFORM} -c ${RELEASE_TYPE} --self-contained`.
While they are handy for the developer, some bad actors have been making use of
it to make streamlined .NET software packages, for which little forensic tooling
is available.

This tool was created because I had a need to extract the contents of a malicious
.NET Core bundle so I could inspect it with dnSpy. However, the usual way of
gaining access to these files is by running the executable, which lets AppHost
dump everything to %TEMP%.

Obviously, this is a bad idea, especially since **AppHost can be replaced during the compile process with an arbitrary executable**.

DNDebundle allows you to dump the contents of .NET Core bundles without changing
them (meeting forensic integrity requirements), and without running them.

## Installation

Requirements:
* Python >= 3.6
* pip
* git
* git lfs (for examples)

```shell
$ pip install https://gitlab.com/N3X15/DNDebundle.git
```

## Usage

```
usage: dndebundle [-h] {extract,info} ...

.NET Core Bundle Forensic Analysis Utility

positional arguments:
  {extract,info}
    extract       Extract the contents of a .NET Core Bundle
    info          List information about a .NET Core Bundle.

optional arguments:
  -h, --help      show this help message and exit
```

### Static Analysis
To list the contents of a bundle:

```shell
$ dndebundle info path/to/sample.exe
```

### Extraction
To dump the contents of a bundle to a given directory:

```shell
$ dndebundle extract path/to/sample.exe extracted/
```

## License
This code is available to you under the [MIT License](./LICENSE).

## FAQ

### WTF is a .NET Core Bundle?
A .NET Core Bundle is a PE executable (.exe) that has a bunch of files crammed into it.  

When run, the EXE starts up the .NET Core AppHost, which dumps all the files to a temporary directory, and then it runs the contained files as a .NET Core environment.

### Why is this in Python?
Because I like Python.

Also, I've found it easier to parse things and make CLI applications with standard switches and subcommands in Python than in C#.

While, in general, it's more difficult to load up C# BinaryWriter binaries in other languages than in C#, I already have a library for reading and writing BinaryWriter-written files for other projects.

### Forensic Questions

#### Does DNDebundle have a forensic mode?
DNDebundle opens files in read-only mode (`rb`).  It should not modify them in any way. You should double-check checksums before and after, however.

#### Is DNDebundle used by law enforcement?
Not that I'm aware of, but they're welcome to. Share your experiences.

#### Are there best practices for using DNDebundle forensically?
Not really, but some rules of thumb:

1. **Follow the chain of custody guidelines** if you are using this for a case.
1. **Back up everything.**
1. **Only run on a computer designated for forensic analysis of suspect executables.** AKA Don't run this on your work PC.
1. **Verify checksums before and after running DNDebundle.**

#### Is DNDebundle tested in court?
**No**t yet.

This is a tool meant for *forensic analysis* of malware and hacking tools, meaning that it isn't destructive and won't change files if used to read samples in-situ. *In theory*, this means
that it *should* pass requirements set forth by the US DoJ and other agencies, but this has not been tested in court, and I only have an introductory course of CJ computer forensics under my belt.

Please file an issue if you are a LEO and need something fixed for it to be useful.

### Politics

#### Why are you helping the police?
I'm not.

This is a tool intended for those of us who like to pick apart malware *for shits and giggles*, and for cybersecurity incident responders (who are generally just IT guys who work at a company).

While I do try to make this compliant with DoJ guidelines, that's because many of the people using it are required to at their place of employment, and that's not always a police department.  Often, it's third-party consultants who are hired to do forensic analysis for criminal court cases, and they *also*, and more frequently, are hired for private work, such as corporate cybersecurity incident response.

My folks are retired cops. That gives me a perfect view into the world of LEOs, and I don't like what I see. I absolutely do not support the current version of American police, and hope that someday, the government pulls their heads from their asses and installs a system of accountability that doesn't involve self-investigation.

I'm not holding my breath, though.
